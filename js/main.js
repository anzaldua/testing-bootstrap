	window.sr = ScrollReveal();
	sr.reveal('.navbar', {
		duration: 2000,
		origin: 'right'
	});

	window.sr = ScrollReveal();
	sr.reveal('.header-content-left', {
		duration: 2000,
		origin: 'top',
		distance: '300px'
	});

	window.sr = ScrollReveal();
	sr.reveal('.header-content-right', {
		duration: 2000,
		origin: 'right',
		distance: '300px'
	});

	window.sr = ScrollReveal();
	sr.reveal('.headerBtn', {
		duration: 2000,
		origin: 'bottom',
		delay: 1000
	});

	window.sr = ScrollReveal();
	sr.reveal('#testimonial', {
		duration: 2000,
		origin: 'left',
		distance: '300px',
		viewFactor: 0.2
	});

	window.sr = ScrollReveal();
	sr.reveal('.info-left', {
		duration: 2000,
		origin: 'left',
		distance: '300px'
	});

	window.sr = ScrollReveal();
	sr.reveal('.info-right', {
		duration: 2000,
		origin: 'right',
		distance: '300px'
	});

	window.sr = ScrollReveal();
	sr.reveal('.contenido-central', {
		duration: 2000,
		origin: 'bottom',
		delay: 1000
	});

	window.sr = ScrollReveal();
	sr.reveal('.fondo-imagen', {
		duration: 2000,
		origin: 'bottom',
		distance: '200px'
	});

	window.sr = ScrollReveal();
	sr.reveal('.carrusel', {
		duration: 2000,
		origin: 'left',
		distance: '200px'
	});


	

	/*=============================================
	=          SCROLL SUAVE           =
	=============================================*/

	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

	/*=============================================
	=            POPPOVER            =
	=============================================*/

	$(function () {
  $('.botonpop').popover({
    container: 'body'
  })
})

	/*=============================================
	=          CARGA DESPUES DE OPRIMIR BOTON           =
	=============================================*/
	
	$(".btnRemove").click(function(){

		$(".btnRemove").remove()
		$(".carga").append(
				'<div class="spinner-grow text-light m-4" role="status">'+
                '<span class="sr-only">Loading...</span>'+
              '</div>');

	})

		$(".btnRemove2").click(function(){

		$(".btnRemove2").remove()
		$(".carga2").append(
				'<div class="spinner-grow text-light m-4" role="status">'+
                '<span class="sr-only">Loading...</span>'+
              '</div>');

	})

		
	
	
	