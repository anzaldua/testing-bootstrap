  <!-- Navigation -->

  <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light">

    <div class="container">

        <a class="navbar-brand" href="#">Bootstrap WebSite</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="
        #navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

          <span class="navbar-toggler-icon"></span>

        </button>

        <div class="collapse navbar-collapse" id="navbarNav">

          <ul class="navbar-nav ml-auto">

            <li class="nav-item active">

              <a class="nav-link" href="#header">Home</a>

            </li>

            <li class="nav-item">

              <a class="nav-link" href="#info-one">About</a>

            </li>

            <li class="nav-item">

              <a class="nav-link" href="#info-two">Pricing</a>

            </li>

            <li class="nav-item">

              <a class="nav-link" href="#contacto">Contactanos</a>

            </li>

          </ul>

        </div>

    </div>

  </nav>

  <!-- HEADER -->

  <header id="header">
    
    <div class="container">
      
      <div class="row">
        
        <div class="col-md-6">

          <div class="header-content-left"><img src="img/radiadores.jpg" style="width:100%">

          </div>
          
        </div>

        <div class="col-md-6">
          
            <div class="header-content-right">
              
              <h1 class="display-4">Lorem ipsum dolor.</h1>
              
              <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad odio veniam, soluta ullam deserunt optio labore necessitatibus? Officiis dolor labore dolorem eum quo nisi. Exercitationem cupiditate sit temporibus alias esse.</p>

              <button type="button" class="botonpop btn btn-lg btn-outline-light headerBtn mt-3" data-placement="right" data-toggle="popover" title="Carro De Lujo" data-content="de los carros mas lujosos y nuevos de la epoca, puede llegar a tener mas de 1,000,000 Kilometros recorridos y su tiempo de vida no ha llegado ni a la mitad">Informacion del vehiculo</button>

              <div class="carga"></div>

              <a href="#"><button type="button" class="btn btn-outline-light btn-lg mt-3 headerBtn btnRemove" data-toggle="tooltip" data-placement="top" title="Informacion a detalle del vehiculo">Read more</button></a>

            </div>

        </div>

      </div>

    </div>

  </header>

  <!-- TESTIMONIAL -->

  <section id="testimonial">
    
    <div class="container">
      
      <p class="h2 mb-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates, autem rerum, dicta culpa nisi ipsa facilis fugit, veritatis odit eveniet molestiae corporis dolor maxime, reprehenderit blanditiis enim ex doloribus excepturi.</p>
      <p class="h4">- Ryan Ray</p>

    </div>

  </section>

  <!-- INFO ONE -->

  <section id="info-one">
    
    <div class="container mt-5">

      <div class="row mb-5">
        
          <div class="col-md-6 my-auto">

            <div class="info-left">

                <h2 class="display-4 mb-3">Lorem ipsum dolor sit</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facere explicabo nisi rerum voluptatibus atque odio consectetur a voluptatum, aut illo eveniet asperiores similique labore dicta perferendis inventore. Maxime, quo.</p>

                <div class="carga2"></div>
        
                <a href="#"><button type="button" class="btn btn-outline-light btn-lg mt-3 headerBtn btnRemove2" data-toggle="tooltip" data-placement="top" title="Informacion a detalle del vehiculo">Read more</button></a>

            </div>

          </div>

          <div class="col-md-6">

            <div class="info-right">

              <img src="img/bugatti-divo.jpg" style="width:100%" alt="buggati">

            </div>

         </div>

        </div>

    </div>

  </section>

  <!-- INFO TWO -->

  <section id="info-two" style="background-color: #f1f1f1">
      
      <div class="container">

        <div class="row my-4 contenido-central">

          <div class="col-md-6 ">

            <h2 class="display-4 ">Lorem ipsum dolor</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem omnis amet atque vero dolorem modi, repellat maiores deserunt aperiam velit dignissimos quas explicabo, porro placeat sint doloremque, mollitia repellendus! Quod.</p>

          </div>

          <div class="col-md-6">

            <h2 class="display-4">Lorem ipsum dolor</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem omnis amet atque vero dolorem modi, repellat maiores deserunt aperiam velit dignissimos quas explicabo, porro placeat sint doloremque, mollitia repellendus! Quod.</p>

          </div>

        </div>

      </div>

  </section>
  
  <section class="carrusel">

    <div class="container">

      <div class="row justify-content-center">

        <div class="col-md-6 mb-5">

          <div class="bd-example">

            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">

                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>

                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>

                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>

              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <img src="img/cadillac.jpg" class="d-block w-100" alt="...">

                  <div class="carousel-caption d-none d-md-block">

                    <h5>Nuevo Cadillac 2019</h5>

                    <p>El mejor y unico carro en el mundo con traccion automatica</p>

                  </div>

                </div>

                <div class="carousel-item">

                  <img src="img/bmw.jpg" class="d-block w-100" alt="...">

                  <div class="carousel-caption d-none d-md-block">

                    <h5>Nuevo BMW 2019</h5>

                    <p>Ninguno se le compara su diseño</p>

                  </div>

                </div>

                <div class="carousel-item">

                  <img src="img/Lamborghini.jpg" class="d-block w-100" alt="...">

                  <div class="carousel-caption d-none d-md-block">

                    <h5>Nuevo Lamborghini 2019</h5>

                    <p>Presentamos el carro mas lujoso del mundo</p>

                  </div>

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">

                <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                <span class="sr-only">Previous</span>

              </a>

              <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">

                <span class="carousel-control-next-icon" aria-hidden="true"></span>

                <span class="sr-only">Next</span>

              </a>

            </div>

          </div>

        </div>

      </div>

    </div>

  </section>

  <!-- VIDEO -->
  <section class="container contenido-central" style="background-color: #f1f1f1">

    <div class="row my-4 justify-content-center">

      <div class="col-md-12 p-5">
          <h1 class="display-4 mb-4 justify-center">Una pequeña demostracion de lo que sabemos hacer</h1> 
        <div class="embed-responsive embed-responsive-21by9">
          
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>

        </div>

      </div>

    </div>

  </section>

  <!-- CONTACTO -->

  <footer id="contacto" style="background-color: #f1f1f1">

    <div class="container my-4 fondo-imagen">

      <div class="row"> 

        <div class="col-md-5">
            <form class="card my-2">

              <div class="card-body">

                <div class="form-group">

                  <input type="text" placeholder="name" class="form-control">

                </div>

                <div class="form-group">

                  <input type="email" placeholder="email" class="form-control">

                </div>

                <div class="form-group">

                  <textarea placeholder="message" cols="30" rows="8" class="form-control"></textarea>

                </div>

                <button type="submit" class="btn btn-outline-secondary btn-block">Enviar</button>

              </div>

            </form>

        </div>

        <div class="col-md-5">
          
          <div class="display-4 mb-4 mt-5 ml-5">Contactanos!!!</div> 

        </div>

      </div>

    </div>

  </footer>